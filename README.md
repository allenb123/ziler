# ziler
Automatic window tiler for EWMH-compliant window managers.

### Installation
1. Clone this repo
2. Clone [clap](https://github.com/Hejsil/zig-clap) and move/symlink to `ziler/clap`
3. Run

```bash
zig build install --prefix /usr
```

### Usage
See `ziler --help` for more information.

Windows will be ordered from the leftmost x-position to the rightmost x-position.
