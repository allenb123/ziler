const std = @import("std");
const builtin = @import("builtin");

pub fn build(b: *std.build.Builder) !void {
    const exe = b.addExecutable("ziler", "src/ziler.zig");

    exe.addPackagePath("clap", "clap/clap.zig");
    exe.linkSystemLibrary("c");
    exe.linkSystemLibrary("xcb");

    exe.setTarget(b.standardTargetOptions(.{}));
    exe.setBuildMode(b.standardReleaseOptions());
    exe.install();

    const run = exe.run();
    run.step.dependOn(b.getInstallStep());

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run.step);
}
