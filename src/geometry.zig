const math = @import("std").math;
const Dimen = @import("dimen.zig").Dimen;

pub const Geometry = struct {
    // Gap between windows
    gap: u32,
    // Screen size, excluding margins
    screen_w: u32,
    screen_h: u32,

    // Margins
    offset_x: i32,
    offset_y: i32,

    fn get_x(self: *const Geometry, col: u32) i32 {
        return @intCast(i32, (math.pow(u32, 2, col) - 1) * (self.screen_w + self.gap) / math.pow(u32, 2, col));
    }

    fn get_y(self: *const Geometry, row: u32) i32 {
        return @intCast(i32, (math.pow(u32, 2, row) - 1) * (self.screen_h + self.gap) / math.pow(u32, 2, row));
    }

    fn get_w(self: *const Geometry, iter: u32) u32 {
        return (self.screen_w - (math.pow(u32, 2, iter) - 1) * self.gap) / math.pow(u32, 2, iter);
    }

    fn get_h(self: *const Geometry, iter: u32) u32 {
        return (self.screen_h - (math.pow(u32, 2, iter) - 1) * self.gap) / math.pow(u32, 2, iter);
    }

    pub fn dimen(self: *const Geometry, item: u32, final: bool) Dimen {
        const col = (item + 1) / 2;
        const row = item / 2;
        var iter_w = item / 2 + 1;
        var iter_h = (item + 1) / 2;
        if (final) {
            iter_w = (item + 1) / 2;
            iter_h = item / 2;
        }
        return Dimen{
            .x = self.get_x(col) + self.offset_x,
            .y = self.get_y(row) + self.offset_y,
            .w = self.get_w(iter_w),
            .h = self.get_h(iter_h),
        };
    }
};

const debug = @import("std").debug;
const testing = @import("std").testing;

test "geometry" {
    const mem = @import("std").mem;
    const conf = Geometry{
        .gap = 16,
        .screen_w = 80,
        .screen_h = 80,
        .offset_x = 0,
        .offset_y = 0,
    };
    //    testing.expect(mem.eql(u32, &conf.dimen(0, false), &Dimen{ 0, 0, 32, 80 }));
    //    testing.expect(mem.eql(u32, &conf.dimen(1, false), &Dimen{ 48, 0, 32, 32 }));
    //    testing.expect(mem.eql(u32, &conf.dimen(2, false), &[_]u32{ 48, 48, 8, 32 }));
}
