const Client = @import("interface.zig").Client;
const Geometry = @import("geometry.zig").Geometry;
const heap = @import("std").heap;
const debug = @import("std").debug;
const sort = @import("std").sort;
const clap = @import("clap");
const io = @import("std").io;
const fmt = @import("std").fmt;

var cl: Client = undefined;

pub fn leftOf(wid1: u32, wid2: u32) bool {
    var d1 = cl.dimen(wid1) catch @panic("failed to get coordinates");
    var d2 = cl.dimen(wid2) catch @panic("failed to get coordinates");
    if (d1.x == d2.x) {
        return d1.y < d2.y;
    }
    return d1.x < d2.x;
}

pub fn main() !void {
    @setEvalBranchQuota(4096);
    const params = comptime [_]clap.Param(clap.Help){
        clap.parseParam("-h, --help Display this help and exit.") catch unreachable,
        clap.parseParam("-g, --gap <GAP> Gap between windows.") catch unreachable,
        clap.parseParam("-t, --top <NUM> Space to leave between the windows and top edge.") catch unreachable,
        clap.parseParam("-b, --bottom <NUM> Space to leave between the windows and bottom edge.") catch unreachable,
        clap.parseParam("-l, --left <NUM> Space to leave between the windows and left edge.") catch unreachable,
        clap.parseParam("-r, --right <NUM> Space to leave between the windows and right edge.") catch unreachable,
    };

    var args = clap.parse(clap.Help, &params, heap.c_allocator) catch {
        const stderr = io.getStdErr().outStream();
        _ = try stderr.write("usage:\n\tziler ");
        try clap.usage(stderr, &params);
        _ = try stderr.write("\n");
        return;
    };
    defer args.deinit();

    if (args.flag("--help")) {
        const stderr = io.getStdErr().outStream();
        _ = try stderr.write("usage:\n\tziler ");
        try clap.usage(stderr, &params);
        _ = try stderr.write("\n");
        _ = try stderr.write("options:\n");
        try clap.help(stderr, &params);
        return;
    }

    try cl.init(heap.c_allocator);
    const size = try cl.screensize();

    var geo: Geometry = .{
        .gap = 0,
        .screen_w = size[0],
        .screen_h = size[1],
        .offset_x = 0,
        .offset_y = 0,
    };

    if (args.option("--gap")) |gap| {
        geo.gap = try fmt.parseInt(u32, gap, 10);
    }
    if (args.option("--top")) |str| {
        const num = try fmt.parseInt(u32, str, 10);
        geo.offset_y = @intCast(i32, num);
        geo.screen_h -= num;
    }
    if (args.option("--bottom")) |str| {
        const num = try fmt.parseInt(u32, str, 10);
        geo.screen_h -= num;
    }
    if (args.option("--left")) |str| {
        const num = try fmt.parseInt(u32, str, 10);
        geo.offset_x = @intCast(i32, num);
        geo.screen_w -= num;
    }
    if (args.option("--right")) |str| {
        const num = try fmt.parseInt(u32, str, 10);
        geo.screen_w -= num;
    }

    const desktop = try cl.desktop();
    const windows = try cl.list(desktop);
    sort.sort(u32, windows, leftOf);

    var i: u32 = 0;
    for (windows) |wid| {
        const dimen = geo.dimen(i, i + 1 == windows.len);
        try cl.move(wid, dimen);
        i += 1;
    }

    cl.deinit();
}
