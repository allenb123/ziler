const c = @import("c.zig");
const mem = @import("std").mem;
const heap = @import("std").heap;
const debug = @import("std").debug;
const Dimen = @import("dimen.zig").Dimen;

const InterfaceError = error{ EWMHNotSupported, Unknown };

/// Interfaces with the X server
pub const Client = struct {
    conn: *c.xcb_connection_t = undefined,
    screen: *c.xcb_screen_t = undefined,
    allocator: *mem.Allocator,

    _NET_CLIENT_LIST: c.xcb_atom_t = undefined,
    _NET_WM_WINDOW_TYPE: c.xcb_atom_t = undefined,
    _NET_WM_WINDOW_TYPE_NORMAL: c.xcb_atom_t = undefined,
    _NET_WM_DESKTOP: c.xcb_atom_t = undefined,
    _NET_CURRENT_DESKTOP: c.xcb_atom_t = undefined,
    _NET_FRAME_EXTENTS: c.xcb_atom_t = undefined,

    pub fn init(self: *Client, allocator: *mem.Allocator) !void {
        self.allocator = allocator;
        self.conn = c.xcb_connect(null, null) orelse return error.FailedToConnect;
        self.screen = c.xcb_setup_roots_iterator(c.xcb_get_setup(self.conn)).data orelse return error.FailedToConnect;

        {
            const cookie = c.xcb_intern_atom(self.conn, 1, "_NET_CLIENT_LIST".len, "_NET_CLIENT_LIST");
            const reply: *c.xcb_intern_atom_reply_t = c.xcb_intern_atom_reply(self.conn, cookie, null) orelse return error.AtomError;
            self._NET_CLIENT_LIST = reply.atom;
            c.free(reply);
        }
        {
            const cookie = c.xcb_intern_atom(self.conn, 1, "_NET_CURRENT_DESKTOP".len, "_NET_CURRENT_DESKTOP");
            const reply: *c.xcb_intern_atom_reply_t = c.xcb_intern_atom_reply(self.conn, cookie, null) orelse return error.AtomError;
            self._NET_CURRENT_DESKTOP = reply.atom;
            c.free(reply);
        }
        {
            const cookie = c.xcb_intern_atom(self.conn, 1, "_NET_WM_DESKTOP".len, "_NET_WM_DESKTOP");
            const reply: *c.xcb_intern_atom_reply_t = c.xcb_intern_atom_reply(self.conn, cookie, null) orelse return error.AtomError;
            self._NET_WM_DESKTOP = reply.atom;
            c.free(reply);
        }
        {
            const cookie = c.xcb_intern_atom(self.conn, 1, "_NET_WM_WINDOW_TYPE".len, "_NET_WM_WINDOW_TYPE");
            const reply: *c.xcb_intern_atom_reply_t = c.xcb_intern_atom_reply(self.conn, cookie, null) orelse return error.AtomError;
            self._NET_WM_WINDOW_TYPE = reply.atom;
            c.free(reply);
        }
        {
            const cookie = c.xcb_intern_atom(self.conn, 1, "_NET_WM_WINDOW_TYPE_NORMAL".len, "_NET_WM_WINDOW_TYPE_NORMAL");
            const reply: *c.xcb_intern_atom_reply_t = c.xcb_intern_atom_reply(self.conn, cookie, null) orelse return error.AtomError;
            self._NET_WM_WINDOW_TYPE_NORMAL = reply.atom;
            c.free(reply);
        }
        {
            const cookie = c.xcb_intern_atom(self.conn, 1, "_NET_FRAME_EXTENTS".len, "_NET_FRAME_EXTENTS");
            const reply: *c.xcb_intern_atom_reply_t = c.xcb_intern_atom_reply(self.conn, cookie, null) orelse return error.AtomError;
            self._NET_FRAME_EXTENTS = reply.atom;
            c.free(reply);
        }
    }

    /// Returns a list of window IDs in the given desktop. The caller is responsible for freeing the memory.
    pub fn list(self: *Client, desktop_: u32) ![]u32 {
        const reply: *c.xcb_get_property_reply_t = c.xcb_get_property_reply(self.conn, c.xcb_get_property(self.conn, 0, self.screen.root, self._NET_CLIENT_LIST, c.XCB_GET_PROPERTY_TYPE_ANY, 0, 32), null) orelse return error.ListError;
        const cptr = c.xcb_get_property_value(reply);
        const ptr = @ptrCast([*]u32, @alignCast(@alignOf([*]u32), cptr));
        const rawlist = ptr[0..reply.length];

        const f_list = self.allocator.alloc(u32, rawlist.len) catch return error.OutOfMemory;
        var f_list_len: u32 = 0;
        for (rawlist) |wid| {
            const desktop_cookie = c.xcb_get_property(self.conn, 0, wid, self._NET_WM_DESKTOP, c.XCB_GET_PROPERTY_TYPE_ANY, 0, 1);
            const type_cookie = c.xcb_get_property(self.conn, 0, wid, self._NET_WM_WINDOW_TYPE, c.XCB_GET_PROPERTY_TYPE_ANY, 0, 1);

            const desktop_reply: *c.xcb_get_property_reply_t = c.xcb_get_property_reply(self.conn, desktop_cookie, null);
            const type_reply: *c.xcb_get_property_reply_t = c.xcb_get_property_reply(self.conn, type_cookie, null);

            const desktop_num: ?u32 = if (desktop_reply.length == 0) null else @ptrCast(*u32, @alignCast(@alignOf(*u32), c.xcb_get_property_value(desktop_reply))).*;
            const type_atom: ?c.xcb_atom_t = if (type_reply.length == 0) null else @ptrCast(*c.xcb_atom_t, @alignCast(@alignOf(*c.xcb_atom_t), c.xcb_get_property_value(type_reply))).*;

            if ((type_atom == null or type_atom.? == self._NET_WM_WINDOW_TYPE_NORMAL) and
                (desktop_num != null and desktop_num.? == desktop_))
            {
                f_list[f_list_len] = wid;
                f_list_len += 1;
            }

            c.free(desktop_reply);
            c.free(type_reply);
        }
        c.free(reply);

        return f_list[0..f_list_len];
    }

    /// Moves and resizes a given window
    pub fn move(self: *Client, wid: u32, dimen_: Dimen) !void {
        const cookie = c.xcb_get_property(self.conn, 0, wid, self._NET_FRAME_EXTENTS, c.XCB_GET_PROPERTY_TYPE_ANY, 0, 4);
        const reply: *c.xcb_get_property_reply_t = c.xcb_get_property_reply(self.conn, cookie, null);
        if (reply.length == 0) {
            return InterfaceError.EWMHNotSupported;
        }
        const array = @ptrCast([*]i32, @alignCast(@alignOf(*i32), c.xcb_get_property_value(reply)));

        const mask: u32 = c.XCB_CONFIG_WINDOW_X | c.XCB_CONFIG_WINDOW_Y | c.XCB_CONFIG_WINDOW_WIDTH | c.XCB_CONFIG_WINDOW_HEIGHT;
        const values = [_]i32{
            dimen_.x, dimen_.y, @intCast(i32, dimen_.w) - array[0] - array[1], @intCast(i32, dimen_.h) - array[2] - array[3],
        };

        const err = c.xcb_request_check(self.conn, c.xcb_configure_window_checked(self.conn, wid, mask, &values));
        if (err != null) {
            debug.warn("x11 interface error: {}", .{err});
            return InterfaceError.Unknown;
        }
    }

    /// Returns the size of the screen
    pub fn screensize(self: *Client) ![2]u32 {
        const cookie = c.xcb_get_geometry(self.conn, self.screen.root);
        const reply: *c.xcb_get_geometry_reply_t = c.xcb_get_geometry_reply(self.conn, cookie, null);
        const ret = [2]u32{ reply.width, reply.height };
        c.free(reply);
        return ret;
    }

    /// Returns the dimensions of a given window
    pub fn dimen(self: *Client, wid: u32) !Dimen {
        const cookie = c.xcb_get_geometry(self.conn, wid);
        const cookie_dec = c.xcb_get_property(self.conn, 0, wid, self._NET_FRAME_EXTENTS, c.XCB_GET_PROPERTY_TYPE_ANY, 0, 4);

        const reply: *c.xcb_get_geometry_reply_t = c.xcb_get_geometry_reply(self.conn, cookie, null);
        const reply_dec: *c.xcb_get_property_reply_t = c.xcb_get_property_reply(self.conn, cookie_dec, null);

        const cookie_tr = c.xcb_translate_coordinates(self.conn, wid, self.screen.root, reply.x, reply.y);
        const reply_tr: *c.xcb_translate_coordinates_reply_t = c.xcb_translate_coordinates_reply(self.conn, cookie_tr, null);

        if (reply_dec.length == 0) {
            return InterfaceError.EWMHNotSupported;
        }
        const dec = @ptrCast([*]i32, @alignCast(@alignOf(*i32), c.xcb_get_property_value(reply_dec)));

        const ret = Dimen{
            .x = reply_tr.dst_x,
            .y = reply_tr.dst_y,
            .w = reply.width + @intCast(u16, dec[0] + dec[1]),
            .h = reply.height + @intCast(u16, dec[2] + dec[3]),
        };
        c.free(reply);
        return ret;
    }

    /// Returns the current desktop
    pub fn desktop(self: *Client) !u32 {
        const cookie = c.xcb_get_property(self.conn, 0, self.screen.root, self._NET_CURRENT_DESKTOP, c.XCB_GET_PROPERTY_TYPE_ANY, 0, 1);
        const reply = c.xcb_get_property_reply(self.conn, cookie, null);
        const val = @ptrCast(*u32, @alignCast(@alignOf(*u32), c.xcb_get_property_value(reply))).*;
        c.free(reply);
        return val;
    }

    /// Disconnects from the server
    pub fn deinit(self: *Client) void {
        c.xcb_disconnect(self.conn);
    }
};

test "client" {
    const debug = @import("std").debug;
    const testing = @import("std").testing;
    var cl: Client = undefined;
    try cl.init(heap.c_allocator);
    const list = try cl.list(0);
    try cl.move(list[0], 10, 10, 230, 230);
    const size = try cl.screensize();
    debug.warn("\nscreen size = {}x{}\n", .{ size[0], size[1] });

    cl.deinit();
}
